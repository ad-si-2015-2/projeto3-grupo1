
package cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SetDeckResponse_QNAME = new QName("http://servidor/", "setDeckResponse");
    private final static QName _GetTurnPlayer_QNAME = new QName("http://servidor/", "getTurnPlayer");
    private final static QName _SetLoserResponse_QNAME = new QName("http://servidor/", "setLoserResponse");
    private final static QName _GetDiscard_QNAME = new QName("http://servidor/", "getDiscard");
    private final static QName _SetPlayersNumberResponse_QNAME = new QName("http://servidor/", "setPlayersNumberResponse");
    private final static QName _SetLoser_QNAME = new QName("http://servidor/", "setLoser");
    private final static QName _StartDeck_QNAME = new QName("http://servidor/", "startDeck");
    private final static QName _AdminResponse_QNAME = new QName("http://servidor/", "adminResponse");
    private final static QName _GetDroppedNumber_QNAME = new QName("http://servidor/", "getDroppedNumber");
    private final static QName _GetPlayersNumberResponse_QNAME = new QName("http://servidor/", "getPlayersNumberResponse");
    private final static QName _SetTurnPlayerResponse_QNAME = new QName("http://servidor/", "setTurnPlayerResponse");
    private final static QName _SetWinner_QNAME = new QName("http://servidor/", "setWinner");
    private final static QName _NotifyAllResponse_QNAME = new QName("http://servidor/", "notifyAllResponse");
    private final static QName _SetTurnPlayer_QNAME = new QName("http://servidor/", "setTurnPlayer");
    private final static QName _SetIds_QNAME = new QName("http://servidor/", "setIds");
    private final static QName _GetCardResponse_QNAME = new QName("http://servidor/", "getCardResponse");
    private final static QName _DealCardsResponse_QNAME = new QName("http://servidor/", "dealCardsResponse");
    private final static QName _DealCards_QNAME = new QName("http://servidor/", "dealCards");
    private final static QName _RegistrarResponse_QNAME = new QName("http://servidor/", "registrarResponse");
    private final static QName _NotifyAll_QNAME = new QName("http://servidor/", "notifyAll");
    private final static QName _GetQtdUsuariosResponse_QNAME = new QName("http://servidor/", "getQtdUsuariosResponse");
    private final static QName _SetStatus_QNAME = new QName("http://servidor/", "setStatus");
    private final static QName _Registrar_QNAME = new QName("http://servidor/", "registrar");
    private final static QName _GetCard_QNAME = new QName("http://servidor/", "getCard");
    private final static QName _SetDiscard_QNAME = new QName("http://servidor/", "setDiscard");
    private final static QName _SetIdsResponse_QNAME = new QName("http://servidor/", "setIdsResponse");
    private final static QName _GetDroppedNumberResponse_QNAME = new QName("http://servidor/", "getDroppedNumberResponse");
    private final static QName _GetDeck_QNAME = new QName("http://servidor/", "getDeck");
    private final static QName _GetStatus_QNAME = new QName("http://servidor/", "getStatus");
    private final static QName _GetStatusResponse_QNAME = new QName("http://servidor/", "getStatusResponse");
    private final static QName _UltimoId_QNAME = new QName("http://servidor/", "ultimoId");
    private final static QName _IncreaseDroppedNumberResponse_QNAME = new QName("http://servidor/", "increaseDroppedNumberResponse");
    private final static QName _SetPlayersNumber_QNAME = new QName("http://servidor/", "setPlayersNumber");
    private final static QName _SetWinnerResponse_QNAME = new QName("http://servidor/", "setWinnerResponse");
    private final static QName _GetQtdUsuarios_QNAME = new QName("http://servidor/", "getQtdUsuarios");
    private final static QName _Admin_QNAME = new QName("http://servidor/", "admin");
    private final static QName _UltimoIdResponse_QNAME = new QName("http://servidor/", "ultimoIdResponse");
    private final static QName _GetTurnPlayerResponse_QNAME = new QName("http://servidor/", "getTurnPlayerResponse");
    private final static QName _GetDeckResponse_QNAME = new QName("http://servidor/", "getDeckResponse");
    private final static QName _GetDiscardResponse_QNAME = new QName("http://servidor/", "getDiscardResponse");
    private final static QName _StartDeckResponse_QNAME = new QName("http://servidor/", "startDeckResponse");
    private final static QName _SetDiscardResponse_QNAME = new QName("http://servidor/", "setDiscardResponse");
    private final static QName _SetDeck_QNAME = new QName("http://servidor/", "setDeck");
    private final static QName _SetStatusResponse_QNAME = new QName("http://servidor/", "setStatusResponse");
    private final static QName _IncreaseDroppedNumber_QNAME = new QName("http://servidor/", "increaseDroppedNumber");
    private final static QName _GetPlayersNumber_QNAME = new QName("http://servidor/", "getPlayersNumber");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SetPlayersNumberResponse }
     * 
     */
    public SetPlayersNumberResponse createSetPlayersNumberResponse() {
        return new SetPlayersNumberResponse();
    }

    /**
     * Create an instance of {@link GetDiscard }
     * 
     */
    public GetDiscard createGetDiscard() {
        return new GetDiscard();
    }

    /**
     * Create an instance of {@link SetLoser }
     * 
     */
    public SetLoser createSetLoser() {
        return new SetLoser();
    }

    /**
     * Create an instance of {@link SetDeckResponse }
     * 
     */
    public SetDeckResponse createSetDeckResponse() {
        return new SetDeckResponse();
    }

    /**
     * Create an instance of {@link GetTurnPlayer }
     * 
     */
    public GetTurnPlayer createGetTurnPlayer() {
        return new GetTurnPlayer();
    }

    /**
     * Create an instance of {@link SetLoserResponse }
     * 
     */
    public SetLoserResponse createSetLoserResponse() {
        return new SetLoserResponse();
    }

    /**
     * Create an instance of {@link SetTurnPlayerResponse }
     * 
     */
    public SetTurnPlayerResponse createSetTurnPlayerResponse() {
        return new SetTurnPlayerResponse();
    }

    /**
     * Create an instance of {@link GetPlayersNumberResponse }
     * 
     */
    public GetPlayersNumberResponse createGetPlayersNumberResponse() {
        return new GetPlayersNumberResponse();
    }

    /**
     * Create an instance of {@link StartDeck }
     * 
     */
    public StartDeck createStartDeck() {
        return new StartDeck();
    }

    /**
     * Create an instance of {@link AdminResponse }
     * 
     */
    public AdminResponse createAdminResponse() {
        return new AdminResponse();
    }

    /**
     * Create an instance of {@link GetDroppedNumber }
     * 
     */
    public GetDroppedNumber createGetDroppedNumber() {
        return new GetDroppedNumber();
    }

    /**
     * Create an instance of {@link SetIds }
     * 
     */
    public SetIds createSetIds() {
        return new SetIds();
    }

    /**
     * Create an instance of {@link GetCardResponse }
     * 
     */
    public GetCardResponse createGetCardResponse() {
        return new GetCardResponse();
    }

    /**
     * Create an instance of {@link SetWinner }
     * 
     */
    public SetWinner createSetWinner() {
        return new SetWinner();
    }

    /**
     * Create an instance of {@link NotifyAllResponse }
     * 
     */
    public NotifyAllResponse createNotifyAllResponse() {
        return new NotifyAllResponse();
    }

    /**
     * Create an instance of {@link SetTurnPlayer }
     * 
     */
    public SetTurnPlayer createSetTurnPlayer() {
        return new SetTurnPlayer();
    }

    /**
     * Create an instance of {@link GetQtdUsuariosResponse }
     * 
     */
    public GetQtdUsuariosResponse createGetQtdUsuariosResponse() {
        return new GetQtdUsuariosResponse();
    }

    /**
     * Create an instance of {@link SetStatus }
     * 
     */
    public SetStatus createSetStatus() {
        return new SetStatus();
    }

    /**
     * Create an instance of {@link DealCardsResponse }
     * 
     */
    public DealCardsResponse createDealCardsResponse() {
        return new DealCardsResponse();
    }

    /**
     * Create an instance of {@link DealCards }
     * 
     */
    public DealCards createDealCards() {
        return new DealCards();
    }

    /**
     * Create an instance of {@link RegistrarResponse }
     * 
     */
    public RegistrarResponse createRegistrarResponse() {
        return new RegistrarResponse();
    }

    /**
     * Create an instance of {@link NotifyAll }
     * 
     */
    public NotifyAll createNotifyAll() {
        return new NotifyAll();
    }

    /**
     * Create an instance of {@link GetDeck }
     * 
     */
    public GetDeck createGetDeck() {
        return new GetDeck();
    }

    /**
     * Create an instance of {@link GetStatus }
     * 
     */
    public GetStatus createGetStatus() {
        return new GetStatus();
    }

    /**
     * Create an instance of {@link GetStatusResponse }
     * 
     */
    public GetStatusResponse createGetStatusResponse() {
        return new GetStatusResponse();
    }

    /**
     * Create an instance of {@link Registrar }
     * 
     */
    public Registrar createRegistrar() {
        return new Registrar();
    }

    /**
     * Create an instance of {@link SetIdsResponse }
     * 
     */
    public SetIdsResponse createSetIdsResponse() {
        return new SetIdsResponse();
    }

    /**
     * Create an instance of {@link SetDiscard }
     * 
     */
    public SetDiscard createSetDiscard() {
        return new SetDiscard();
    }

    /**
     * Create an instance of {@link GetCard }
     * 
     */
    public GetCard createGetCard() {
        return new GetCard();
    }

    /**
     * Create an instance of {@link GetDroppedNumberResponse }
     * 
     */
    public GetDroppedNumberResponse createGetDroppedNumberResponse() {
        return new GetDroppedNumberResponse();
    }

    /**
     * Create an instance of {@link UltimoId }
     * 
     */
    public UltimoId createUltimoId() {
        return new UltimoId();
    }

    /**
     * Create an instance of {@link GetTurnPlayerResponse }
     * 
     */
    public GetTurnPlayerResponse createGetTurnPlayerResponse() {
        return new GetTurnPlayerResponse();
    }

    /**
     * Create an instance of {@link GetDeckResponse }
     * 
     */
    public GetDeckResponse createGetDeckResponse() {
        return new GetDeckResponse();
    }

    /**
     * Create an instance of {@link SetPlayersNumber }
     * 
     */
    public SetPlayersNumber createSetPlayersNumber() {
        return new SetPlayersNumber();
    }

    /**
     * Create an instance of {@link IncreaseDroppedNumberResponse }
     * 
     */
    public IncreaseDroppedNumberResponse createIncreaseDroppedNumberResponse() {
        return new IncreaseDroppedNumberResponse();
    }

    /**
     * Create an instance of {@link SetWinnerResponse }
     * 
     */
    public SetWinnerResponse createSetWinnerResponse() {
        return new SetWinnerResponse();
    }

    /**
     * Create an instance of {@link UltimoIdResponse }
     * 
     */
    public UltimoIdResponse createUltimoIdResponse() {
        return new UltimoIdResponse();
    }

    /**
     * Create an instance of {@link Admin }
     * 
     */
    public Admin createAdmin() {
        return new Admin();
    }

    /**
     * Create an instance of {@link GetQtdUsuarios }
     * 
     */
    public GetQtdUsuarios createGetQtdUsuarios() {
        return new GetQtdUsuarios();
    }

    /**
     * Create an instance of {@link SetStatusResponse }
     * 
     */
    public SetStatusResponse createSetStatusResponse() {
        return new SetStatusResponse();
    }

    /**
     * Create an instance of {@link GetPlayersNumber }
     * 
     */
    public GetPlayersNumber createGetPlayersNumber() {
        return new GetPlayersNumber();
    }

    /**
     * Create an instance of {@link IncreaseDroppedNumber }
     * 
     */
    public IncreaseDroppedNumber createIncreaseDroppedNumber() {
        return new IncreaseDroppedNumber();
    }

    /**
     * Create an instance of {@link GetDiscardResponse }
     * 
     */
    public GetDiscardResponse createGetDiscardResponse() {
        return new GetDiscardResponse();
    }

    /**
     * Create an instance of {@link StartDeckResponse }
     * 
     */
    public StartDeckResponse createStartDeckResponse() {
        return new StartDeckResponse();
    }

    /**
     * Create an instance of {@link SetDiscardResponse }
     * 
     */
    public SetDiscardResponse createSetDiscardResponse() {
        return new SetDiscardResponse();
    }

    /**
     * Create an instance of {@link SetDeck }
     * 
     */
    public SetDeck createSetDeck() {
        return new SetDeck();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDeckResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setDeckResponse")
    public JAXBElement<SetDeckResponse> createSetDeckResponse(SetDeckResponse value) {
        return new JAXBElement<SetDeckResponse>(_SetDeckResponse_QNAME, SetDeckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTurnPlayer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getTurnPlayer")
    public JAXBElement<GetTurnPlayer> createGetTurnPlayer(GetTurnPlayer value) {
        return new JAXBElement<GetTurnPlayer>(_GetTurnPlayer_QNAME, GetTurnPlayer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetLoserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setLoserResponse")
    public JAXBElement<SetLoserResponse> createSetLoserResponse(SetLoserResponse value) {
        return new JAXBElement<SetLoserResponse>(_SetLoserResponse_QNAME, SetLoserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDiscard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getDiscard")
    public JAXBElement<GetDiscard> createGetDiscard(GetDiscard value) {
        return new JAXBElement<GetDiscard>(_GetDiscard_QNAME, GetDiscard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPlayersNumberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setPlayersNumberResponse")
    public JAXBElement<SetPlayersNumberResponse> createSetPlayersNumberResponse(SetPlayersNumberResponse value) {
        return new JAXBElement<SetPlayersNumberResponse>(_SetPlayersNumberResponse_QNAME, SetPlayersNumberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetLoser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setLoser")
    public JAXBElement<SetLoser> createSetLoser(SetLoser value) {
        return new JAXBElement<SetLoser>(_SetLoser_QNAME, SetLoser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartDeck }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "startDeck")
    public JAXBElement<StartDeck> createStartDeck(StartDeck value) {
        return new JAXBElement<StartDeck>(_StartDeck_QNAME, StartDeck.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "adminResponse")
    public JAXBElement<AdminResponse> createAdminResponse(AdminResponse value) {
        return new JAXBElement<AdminResponse>(_AdminResponse_QNAME, AdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDroppedNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getDroppedNumber")
    public JAXBElement<GetDroppedNumber> createGetDroppedNumber(GetDroppedNumber value) {
        return new JAXBElement<GetDroppedNumber>(_GetDroppedNumber_QNAME, GetDroppedNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlayersNumberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getPlayersNumberResponse")
    public JAXBElement<GetPlayersNumberResponse> createGetPlayersNumberResponse(GetPlayersNumberResponse value) {
        return new JAXBElement<GetPlayersNumberResponse>(_GetPlayersNumberResponse_QNAME, GetPlayersNumberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetTurnPlayerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setTurnPlayerResponse")
    public JAXBElement<SetTurnPlayerResponse> createSetTurnPlayerResponse(SetTurnPlayerResponse value) {
        return new JAXBElement<SetTurnPlayerResponse>(_SetTurnPlayerResponse_QNAME, SetTurnPlayerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetWinner }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setWinner")
    public JAXBElement<SetWinner> createSetWinner(SetWinner value) {
        return new JAXBElement<SetWinner>(_SetWinner_QNAME, SetWinner.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotifyAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "notifyAllResponse")
    public JAXBElement<NotifyAllResponse> createNotifyAllResponse(NotifyAllResponse value) {
        return new JAXBElement<NotifyAllResponse>(_NotifyAllResponse_QNAME, NotifyAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetTurnPlayer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setTurnPlayer")
    public JAXBElement<SetTurnPlayer> createSetTurnPlayer(SetTurnPlayer value) {
        return new JAXBElement<SetTurnPlayer>(_SetTurnPlayer_QNAME, SetTurnPlayer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetIds }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setIds")
    public JAXBElement<SetIds> createSetIds(SetIds value) {
        return new JAXBElement<SetIds>(_SetIds_QNAME, SetIds.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getCardResponse")
    public JAXBElement<GetCardResponse> createGetCardResponse(GetCardResponse value) {
        return new JAXBElement<GetCardResponse>(_GetCardResponse_QNAME, GetCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DealCardsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "dealCardsResponse")
    public JAXBElement<DealCardsResponse> createDealCardsResponse(DealCardsResponse value) {
        return new JAXBElement<DealCardsResponse>(_DealCardsResponse_QNAME, DealCardsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DealCards }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "dealCards")
    public JAXBElement<DealCards> createDealCards(DealCards value) {
        return new JAXBElement<DealCards>(_DealCards_QNAME, DealCards.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "registrarResponse")
    public JAXBElement<RegistrarResponse> createRegistrarResponse(RegistrarResponse value) {
        return new JAXBElement<RegistrarResponse>(_RegistrarResponse_QNAME, RegistrarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotifyAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "notifyAll")
    public JAXBElement<NotifyAll> createNotifyAll(NotifyAll value) {
        return new JAXBElement<NotifyAll>(_NotifyAll_QNAME, NotifyAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQtdUsuariosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getQtdUsuariosResponse")
    public JAXBElement<GetQtdUsuariosResponse> createGetQtdUsuariosResponse(GetQtdUsuariosResponse value) {
        return new JAXBElement<GetQtdUsuariosResponse>(_GetQtdUsuariosResponse_QNAME, GetQtdUsuariosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setStatus")
    public JAXBElement<SetStatus> createSetStatus(SetStatus value) {
        return new JAXBElement<SetStatus>(_SetStatus_QNAME, SetStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Registrar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "registrar")
    public JAXBElement<Registrar> createRegistrar(Registrar value) {
        return new JAXBElement<Registrar>(_Registrar_QNAME, Registrar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getCard")
    public JAXBElement<GetCard> createGetCard(GetCard value) {
        return new JAXBElement<GetCard>(_GetCard_QNAME, GetCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDiscard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setDiscard")
    public JAXBElement<SetDiscard> createSetDiscard(SetDiscard value) {
        return new JAXBElement<SetDiscard>(_SetDiscard_QNAME, SetDiscard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetIdsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setIdsResponse")
    public JAXBElement<SetIdsResponse> createSetIdsResponse(SetIdsResponse value) {
        return new JAXBElement<SetIdsResponse>(_SetIdsResponse_QNAME, SetIdsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDroppedNumberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getDroppedNumberResponse")
    public JAXBElement<GetDroppedNumberResponse> createGetDroppedNumberResponse(GetDroppedNumberResponse value) {
        return new JAXBElement<GetDroppedNumberResponse>(_GetDroppedNumberResponse_QNAME, GetDroppedNumberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeck }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getDeck")
    public JAXBElement<GetDeck> createGetDeck(GetDeck value) {
        return new JAXBElement<GetDeck>(_GetDeck_QNAME, GetDeck.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getStatus")
    public JAXBElement<GetStatus> createGetStatus(GetStatus value) {
        return new JAXBElement<GetStatus>(_GetStatus_QNAME, GetStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getStatusResponse")
    public JAXBElement<GetStatusResponse> createGetStatusResponse(GetStatusResponse value) {
        return new JAXBElement<GetStatusResponse>(_GetStatusResponse_QNAME, GetStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UltimoId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "ultimoId")
    public JAXBElement<UltimoId> createUltimoId(UltimoId value) {
        return new JAXBElement<UltimoId>(_UltimoId_QNAME, UltimoId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseDroppedNumberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "increaseDroppedNumberResponse")
    public JAXBElement<IncreaseDroppedNumberResponse> createIncreaseDroppedNumberResponse(IncreaseDroppedNumberResponse value) {
        return new JAXBElement<IncreaseDroppedNumberResponse>(_IncreaseDroppedNumberResponse_QNAME, IncreaseDroppedNumberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPlayersNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setPlayersNumber")
    public JAXBElement<SetPlayersNumber> createSetPlayersNumber(SetPlayersNumber value) {
        return new JAXBElement<SetPlayersNumber>(_SetPlayersNumber_QNAME, SetPlayersNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetWinnerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setWinnerResponse")
    public JAXBElement<SetWinnerResponse> createSetWinnerResponse(SetWinnerResponse value) {
        return new JAXBElement<SetWinnerResponse>(_SetWinnerResponse_QNAME, SetWinnerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQtdUsuarios }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getQtdUsuarios")
    public JAXBElement<GetQtdUsuarios> createGetQtdUsuarios(GetQtdUsuarios value) {
        return new JAXBElement<GetQtdUsuarios>(_GetQtdUsuarios_QNAME, GetQtdUsuarios.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Admin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "admin")
    public JAXBElement<Admin> createAdmin(Admin value) {
        return new JAXBElement<Admin>(_Admin_QNAME, Admin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UltimoIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "ultimoIdResponse")
    public JAXBElement<UltimoIdResponse> createUltimoIdResponse(UltimoIdResponse value) {
        return new JAXBElement<UltimoIdResponse>(_UltimoIdResponse_QNAME, UltimoIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTurnPlayerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getTurnPlayerResponse")
    public JAXBElement<GetTurnPlayerResponse> createGetTurnPlayerResponse(GetTurnPlayerResponse value) {
        return new JAXBElement<GetTurnPlayerResponse>(_GetTurnPlayerResponse_QNAME, GetTurnPlayerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeckResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getDeckResponse")
    public JAXBElement<GetDeckResponse> createGetDeckResponse(GetDeckResponse value) {
        return new JAXBElement<GetDeckResponse>(_GetDeckResponse_QNAME, GetDeckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDiscardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getDiscardResponse")
    public JAXBElement<GetDiscardResponse> createGetDiscardResponse(GetDiscardResponse value) {
        return new JAXBElement<GetDiscardResponse>(_GetDiscardResponse_QNAME, GetDiscardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartDeckResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "startDeckResponse")
    public JAXBElement<StartDeckResponse> createStartDeckResponse(StartDeckResponse value) {
        return new JAXBElement<StartDeckResponse>(_StartDeckResponse_QNAME, StartDeckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDiscardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setDiscardResponse")
    public JAXBElement<SetDiscardResponse> createSetDiscardResponse(SetDiscardResponse value) {
        return new JAXBElement<SetDiscardResponse>(_SetDiscardResponse_QNAME, SetDiscardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDeck }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setDeck")
    public JAXBElement<SetDeck> createSetDeck(SetDeck value) {
        return new JAXBElement<SetDeck>(_SetDeck_QNAME, SetDeck.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "setStatusResponse")
    public JAXBElement<SetStatusResponse> createSetStatusResponse(SetStatusResponse value) {
        return new JAXBElement<SetStatusResponse>(_SetStatusResponse_QNAME, SetStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseDroppedNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "increaseDroppedNumber")
    public JAXBElement<IncreaseDroppedNumber> createIncreaseDroppedNumber(IncreaseDroppedNumber value) {
        return new JAXBElement<IncreaseDroppedNumber>(_IncreaseDroppedNumber_QNAME, IncreaseDroppedNumber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlayersNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://servidor/", name = "getPlayersNumber")
    public JAXBElement<GetPlayersNumber> createGetPlayersNumber(GetPlayersNumber value) {
        return new JAXBElement<GetPlayersNumber>(_GetPlayersNumber_QNAME, GetPlayersNumber.class, null, value);
    }

}
