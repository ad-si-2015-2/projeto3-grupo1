package servidor;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;

public class Cliente {

    private String name;						//Nome do jogador
    private int id;								//Identificador do jogador

    public Cliente() {
        this.id = id;
        this.name = "Sem nome";
    }

    public String getName() throws RemoteException {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setID(int id) {
        this.id = id;
    }

    public void notify(String message) throws RemoteException {
        System.out.println(message);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
