package servidor;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.jws.WebService;

@WebService
public class JogoCopoDAgua {

    //final static int STATUS_JOGO_AGUARDANDO = 1;
    //final static int STATUS_JOGO_ATIVO = 2;
    //final static int STATUS_JOGO_FINALIZADO = 3;
    List<Cliente> clients = new ArrayList();
    private ArrayList<String> deck;		//Baralho
    private String discard;							//Carta descartada
    private int playersNumber = 0;					//Numero de jogadores    
    private int turnPlayer = 1;						//Jogador da vez   
    private int status = 0;							//Status do jogo (valor 1, 2 ou 3)
    private int droppedNumber = 0;				//Numero de batedores
    private int ids;

    public JogoCopoDAgua() {
        status = 0;
        deck = new ArrayList<String>();
    }

    public void registrar(Cliente cliente) {
        clients.add(cliente);
    }

    public int getQtdUsuarios() {
        return clients.size();
    }

    public int admin() {
        for (Cliente c : clients) {
            return c.getId();
        }
        return 0;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int ultimoId() {
        return ids;
    }

    public int setIds() {
        ids += 1;
        return ids;
    }

    public int getPlayersNumber() throws RemoteException {
        return playersNumber;
    }

    public void setPlayersNumber(int number) throws RemoteException {

        if (number <= 13 || number > 2) {
            this.playersNumber = number;
        } else {
            this.playersNumber = 13;
        }
    }

    /**
     * Notifica todos os jogadores.
     *
     * @param message
     * @throws RemoteException
     */
    public void notifyAll(String message) throws RemoteException {
        for (Cliente client : clients) {
            client.notify(message);
        }
    }

    public void startDeck() throws RemoteException {

        deck.clear();
        deck.add("#");

        for (int i = 1; i <= this.playersNumber; i++) {

            deck.add(i + "O");
            deck.add(i + "E");
            deck.add(i + "C");
            deck.add(i + "P");
        }

        setDiscard(getCard());
    }

    public ArrayList<String> getDeck() {
        return deck;
    }

    public void setDeck(ArrayList<String> deck) {
        this.deck = deck;
    }

    public String getCard() {

        Random rd = new Random();
        return deck.remove(rd.nextInt(deck.size()));
    }

    public void dealCards() throws RemoteException {

    }

    public int getTurnPlayer() throws RemoteException {
        return this.turnPlayer;
    }

    public void setTurnPlayer(int playerID) throws RemoteException {
        System.out.println("Indice proximo jogador: " + turnPlayer);
        turnPlayer = playerID;
    }

    public String getDiscard() {
        return this.discard;
    }

    public void setDiscard(String descarte) throws RemoteException {
        this.discard = descarte;
    }

    public void setWinner(int playerID) throws RemoteException {

        this.status = 2;
        //notifyAll("O jogador " + clients.get(playerID).getName() + " venceu o jogo. Batedores em suas posicoes!");
    }

    public void setLoser(int playerID) throws RemoteException {
        this.status = 3;
        notifyAll("O jogador " + clients.get(playerID).getName() + " perdeu o jogo. Jogo finalizado!");
    }

    public void increaseDroppedNumber() throws RemoteException {
        droppedNumber++;
    }

    public int getDroppedNumber() throws RemoteException {
        return droppedNumber;
    }

}
