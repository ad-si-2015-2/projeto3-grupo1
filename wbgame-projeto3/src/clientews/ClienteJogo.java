package clientews;

import cliente.Cliente;
import cliente.JogoCopoDAgua;
import cliente.JogoCopoDAguaService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClienteJogo extends Thread {

    int status;
    String nome;
    static JogoCopoDAgua port;
    static BufferedReader reader;
    static Cliente cliente;
    static List<String> pocketCards;
    static boolean dropped;
    static boolean jogando = false;
    static int numeroDesafio;
    static int id;

    public ClienteJogo(int status) {
        this.status = status;
    }

    public void printPocketCards() throws RemoteException {
        int contador = 1;
        for (String a : pocketCards) {
            System.out.println("Carta: " + contador + ": " + a);
            contador++;
        }
    }

    public void addCard(String card) {
        pocketCards.add(card);
    }

    public boolean verifyWinner() throws RemoteException {

        boolean winner = true;
        int index = Integer.parseInt(pocketCards.get(0).substring(0, pocketCards.get(0).length() - 1));

        for (String card : pocketCards) {
            if (Integer.parseInt(card.substring(0, card.length() - 1)) != index) {
                winner = false;
            }
        }

        if (winner) {
            this.dropped = true;
            port.increaseDroppedNumber();
            port.setWinner(id);
        }
        return winner;
    }

    private int getIndex() throws IOException {

        int discardIndex = 0;
        try {
            discardIndex = Integer.parseInt(read()) - 1;
        } catch (NumberFormatException ex) {
            System.out.println("Voce nao digitou um numero valido. Tente novamente!");
            discardIndex = getIndex();
        }

        if (discardIndex < 0 || discardIndex > 3) {
            System.out.println("Indice invalido. O numero deve ser entre 1 e 4! Digite novamente:");
            discardIndex = getIndex();
        }

        return discardIndex;
    }

    public static void main(String[] args) {
        JogoCopoDAguaService jas = new JogoCopoDAguaService();
        port = jas.getPort(JogoCopoDAgua.class);
        System.out.println("Informe seu nome");
        cliente = new Cliente();
        String comando = read();
        cliente.setName(comando);

        id = port.setIds();
        cliente.setId(id);
        jogando = false;
        new ClienteJogo(0).start();
        if (port.getStatus() == 0) {
            port.registrar(cliente);
            jogando = true;
        } else {
            System.out.println("Aguarde iniciar uma nova partida para poder entrar no jogo.");
            while (port.getStatus() != 0);
            if (port.getQtdUsuarios() == 0) {
                System.out.println("Novo jogo");
            } else {
                System.out.println("Nova partida encontrada.");
            }
            jogando = true;
            port.registrar(cliente);

        }
        new ClienteJogo(1).start();
    }

    public void run() {
        if (status == 0) {
            while (true) {
                try {
                    Thread.sleep(1000 * 3);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClienteJogo.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception e) {
                }
            }
        } else {
            while (true) {
                if (port.getStatus() == 0) {
                    if (port.admin() == id) {
                        System.out.println("Você é o Game Manager, por favor, defina a quantidade de jogadores (3 - 13)");
                        String comando = read();
                        port.setPlayersNumber(Integer.parseInt(comando));

                        System.out.println("Quantidade de jogadores definida: " + port.getPlayersNumber());
                        port.startDeck();

                        System.out.println(gameMaster());
                        comando = read();
                        while (!comando.equals("/i")) {
                            System.out.print(gameMaster());
                            comando = read();
                        }

                        port.setStatus(1);

                    } else {
                        System.out.println("Aguardando o administrador iniciar o jogo.");
                        boolean espera = true;
                        while (espera) {
                            if (port.getStatus() != 0) {
                                espera = false;
                            }
                            if (port.admin() == id) {
                                espera = false;
                            }
                        }

                    }
                    pocketCards = new ArrayList<>();
                    for (int j = 1; j <= 4; j++) {		//4 cartas para cada
                        pocketCards.add(port.getCard());
                    }
                }
                if (port.getStatus() == 1) {

                    try {
                        String answer = "";
                        System.out.println("O jogo foi iniciado!\nSuas cartas sao:\n");
                        printPocketCards();
                        try {
                            System.out.println("Eu: " + id);
                            System.out.println("Jogador da Vez : " + port.getTurnPlayer());
                            if (id == port.getTurnPlayer()) {		//Verifica qual jogador eh a vez

                                System.out.println("Sua vez de jogar!");
                                System.out.println(port.getDiscard());
                                int discardIndex = -1;					//Indice da carta a ser descartada 

                                if (port.getDiscard().equals("#")) {
                                    System.out.println("Azar! A carta descartada eh o curinga. Voce deve mante-la por uma rodada.\n"
                                            + "Digite o indice da carta a ser trocada (1-4):");
                                    printPocketCards();
                                    discardIndex = getIndex();
                                } else {

                                    System.out.println("Descarte do ultimo jogador: " + port.getDiscard()
                                            + "\nDigite /acc para aceitar o descarte ou /pass para passar a vez.");
                                    answer = read();

                                    if (answer.equals("/acc")) {	//Descarte aceito
                                        System.out.println("Descarte aceito. Digite o indice da carta a ser descartada (1-4):");
                                        printPocketCards();
                                        discardIndex = getIndex();
                                    } else if (answer.equals("/pass")) {
                                        System.out.println("Voce passou a vez. Aguarda a proxima rodada!");
                                    }

                                }

                                if (discardIndex != -1) {
                                    pocketCards.add(port.getDiscard());
                                    port.setDiscard(pocketCards.remove(discardIndex));
                                    System.out.println("Suas cartas no momento sao:");
                                    printPocketCards();
                                    
                                }

                                if (!verifyWinner()) { //Define novo jogador da vez
                                    System.out.println("Eu:" + id);
                                    System.out.println("Jogadores:" + port.getPlayersNumber());
                                    if (id == port.getPlayersNumber()) {
                                        port.setTurnPlayer(1);
                                    } else {
                                        port.setTurnPlayer((id + 1));
                                    }
                                    read();
                                    //port.notifyAll("Aperte enter para verificar se eh sua vez!");
                                }

                            } else {
                                System.out.println("Aguarde a sua vez!");
                                read();

                            }

                        } catch (IOException ex) {
                        }

                        if (port.getStatus() == 2) {
                            if (!dropped) {				//Para os jogadores que nao bateram

                                System.out.println("ATENCAO JOGADORES QUE NAO GANHARAM! Digitem /bater para nao perder! O ultimo sera o perdedor!");
                                answer = read();

                                if (answer.equals("/bater")) {

                                    if (port.getDroppedNumber() == port.getPlayersNumber() - 1) {			//Ultimo jogador a bater                                    

                                        System.out.println("Voce foi o ultimo a bater! Voce PERDEU o jogo!");
                                        port.setLoser(id);
                                    } else {
                                        dropped = true;
                                        port.increaseDroppedNumber();
                                        System.out.println("Voce bateu!");
                                    }
                                }
                            } else {
                                System.out.println("Aguarde o fim do jogo!");
                                read();
                            }
                        }

                    } catch (Exception ex) {
                    }
                }
            }
        }
    }

    public String gameMaster() {
        String msg = "";
        msg += "Digite:\n'/i' para começar o jogo\n";
        return msg;
    }

    public static String read() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            return reader.readLine().trim();
        } catch (IOException e) {
            System.out.println("Erro ao entrar com texto");
            return "";
        }
    }
}
